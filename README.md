# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This repository contain the solutions to the Bioinformatics problems on the [rosalind](http://rosalind.info/problems/locations/) website. 

### How do I get set up? ###

* Require Python software to execute the scripts.
* If any additional packages are required, they are explicitly mentioned in the scripts.
* Different Inputs have been tested for, available in the data folder and the corresponding output in output folder.

### Who do I talk to? ###

You can provide your comments, suggestions to pradeep(dot)eranti(at)aalto(dot)fi.