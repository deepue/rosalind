'''
A solution to a "code challenges" that accompanies Bioinformatics Algorithms: An Active-Learning Approach by Phillip Compeau & Pavel Pevzner.
Problem Title: Clump Finding Problem
Problem ID: BA1E
URL: http://rosalind.info/problems/ba1e/
'''

def CheckClumpLength(indices, t, L):
	''' To check whether a given set of t k-mers falls within a clump of size L.'''
	for i in  range(len(indices)-t+1):
		if indices[t+i-1] - indices[i] <= L:
			return True
	return False

# Reading the input data
with open('data/Textbook_BA1E_IP2.txt') as input_data:
	dna, [k, L, t] = [line.strip() if index == 0 else map(int, line.strip().split()) for index, line in enumerate(input_data.readlines())]

# Find all k-mers frequencies with their corresponding indices. 
kmer_dict = dict()
for i in range(len(dna)-k+1):
	if dna[i:i+k] in kmer_dict:
		kmer_dict[dna[i:i+k]][0] += 1
		kmer_dict[dna[i:i+k]][1].append(i)
	else:
		kmer_dict[dna[i:i+k]] = [1, [i]]

# The candidate k-mers which appear at least t times along with their indices.
kmer_candidates = [ [kmer[0],kmer[1][1]] for kmer in kmer_dict.items() if kmer[1][0] >= t]

# To check whether at least t occurences of the candidate k-mer falls within a clump of size L.
kmer_clumps = []
for candidate in kmer_candidates:
	if CheckClumpLength(candidate[1], t, L):
		kmer_clumps.append(candidate[0])

# Print the output
print(' '.join(kmer_clumps))

# Save the output
with open('output/Textbook_BA1E_OP2.txt', 'w') as output_data:
	output_data.write(' '.join(kmer_clumps))
