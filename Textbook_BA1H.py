'''
A solution to a "code challenges" that accompanies Bioinformatics Algorithms: An Active-Learning Approach by Phillip Compeau & Pavel Pevzner.
Problem Title: Approximate Pattern Matching Problem
Problem ID: BA1H
URL: http://rosalind.info/problems/ba1h/
'''

from Textbook_BA1G import hamming_distance

def approx_pattern_match(seq1, seq2, d):
    'Returns the Patterns Approximately matching with the dna sequence with atmost d mismatches.'
    approx_matches = []
    for i in range(len(seq1)-len(seq2)+1):
        if hamming_distance(seq1[i:i+len(seq2)], seq2) <= d:
            approx_matches.append(str(i))
    return approx_matches
    
def main():
    """Main call to Parse, execute the functions, saves problem specific data."""
    # Read the input data.
    with open('data/Textbook_BA1H_IP1.txt') as input_data:
        pattern, dna, n = [line.strip() if index != 2 else int(line.strip()) for index, line in enumerate(input_data.readlines())]

    # Calculate the Approximate Pattern Matches.
    approx_match = approx_pattern_match(dna, pattern, n)
			
    # Print the Output.
    print(' '.join(approx_match))
	
    # Save the output
    with open('output/Textbook_BA1H_OP1.txt', 'w') as output_data:
        output_data.write(' '.join(approx_match))

if __name__ == '__main__':
    main()
