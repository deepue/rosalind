'''
A solution to a "code challenges" that accompanies Bioinformatics Algorithms: An Active-Learning Approach by Phillip Compeau & Pavel Pevzner.
Problem Title: Frequent Words with Mismatches Problem
Problem ID: BA1I
URL: http://rosalind.info/problems/ba1i/
'''

from collections import defaultdict
from itertools import combinations, product, izip

def freq_words_with_mismatches(seq, k, d):
    """Returns all most frequent k-mers with atmost d mismatches in the dna sequence."""
    # Frequency analysis to avoid generating mismatches for the same k-mer more than once.
    kmer_freq = defaultdict(int)
    for i in range(len(seq)-k+1):
        kmer_freq[seq[i:i+k]] += 1

    # Get all of the mismatches for each unique k-mer in the sequence, appearing freq times.
    mismatch_count = defaultdict(int)
    for kmer, freq in kmer_freq.iteritems():
        for mismatch in kmer_mismatches(kmer, d):
            mismatch_count[mismatch] += freq

    # Computing the maximum value 
    max_count = max(mismatch_count.values())
    return sorted([kmer for kmer, count in mismatch_count.iteritems() if count == max_count])


def kmer_mismatches(kmer, d):
    """Returns all k-mers with atmost d mismatches for the given k-mer."""
    # Initialize mismatches with the k-mer itself (i.e. d=0).
    mismatches = [kmer]
    
    # Possible alternate bases for each base
    alt_bases = {'A':'CGT', 'C':'AGT', 'G':'ACT', 'T':'ACG'}

    # Generate k-mers with atmost d mismatches
    for dist in range(1, d+1): # dist = Hamming distance
        for change_indices in combinations(range(len(kmer)), dist): # Identify possible mutation sites
            for substitutions in product(*[alt_bases[kmer[i]] for i in change_indices]): # Alternate bases at current mutation point
                new_mistmatch = list(kmer)
                for idx, sub in izip(change_indices, substitutions):
                    new_mistmatch[idx] = sub # Mutate the base
                mismatches.append(''.join(new_mistmatch))
    return mismatches


def main():
    """Main call to Parse, execute the functions, saves problem specific data."""
    # Read the input data.
    with open('data/Textbook_BA1I_IP2.txt') as input_data:
        seq, [k, d] = [line.strip() if index == 0 else map(int, line.strip().split()) for index, line in enumerate(input_data.readlines())]

    # Get the most frequent k-mers with up to d mismatches.
    most_freq_kmers = freq_words_with_mismatches(seq, k, d)

    # Print the Output.
    print (' '.join(most_freq_kmers))

    # Save the Output.
    with open('output/Textbook_BA1I_OP2.txt', 'w') as output_data:
        output_data.write(' '.join(most_freq_kmers))

if __name__ == '__main__':
    main()
