'''
A solution to a "code challenges" that accompanies Bioinformatics Algorithms: An Active-Learning Approach by Phillip Compeau & Pavel Pevzner.
Problem Title: Frequent Words with Mismatches and Reverse Complements Problem
Problem ID: BA1J
URL: http://rosalind.info/problems/ba1j/
'''

from collections import defaultdict
from Textbook_BA1C import ReverseComplementDNA
from Textbook_BA1I import kmer_mismatches


def freq_words_with_mm_and_rev_comp(seq, k, d):
    """Returns all most frequent k-mers with atmost d mismatches in the dna sequence."""
    # Frequency analysis to avoid generating mismatches for the same k-mer more than once.
    kmer_freq = defaultdict(int)
    for i in xrange(len(seq)-k+1):
        kmer_freq[seq[i:i+k]] += 1
        kmer_freq[ReverseComplementDNA(seq[i:i+k])] += 1

    # Get all of the mismatches for each unique k-mer in the sequence, appearing freq times.
    mismatch_count = defaultdict(int)
    for kmer, freq in kmer_freq.iteritems():
        for mismatch in kmer_mismatches(kmer, d):
            mismatch_count[mismatch] += freq

    # Computing the maximum value
    max_count = max(mismatch_count.values())
    return sorted([kmer for kmer, count in mismatch_count.iteritems() if count == max_count])


def main():
    """Main call to Parse, execute the functions, saves problem specific data."""
    # Read the input data.
    with open('data/Textbook_BA1J_IP2.txt') as input_data:
        seq, [k, d] = [line.strip() if index == 0 else map(int, line.strip().split()) for index, line in enumerate(input_data.readlines())]

    # Get the most frequent k-mers with up to d mismatches.
    most_freq_kmers = freq_words_with_mm_and_rev_comp(seq, k, d)

    # Print the Output.
    print ' '.join(most_freq_kmers)
	
	# Save the Output.
    with open('output/Textbook_BA1J_OP2.txt', 'w') as output_data:
        output_data.write(' '.join(most_freq_kmers))

if __name__ == '__main__':
    main()
