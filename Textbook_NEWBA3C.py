'''
A solution to a "code challenges" that accompanies Bioinformatics Algorithms: An Active-Learning Approach by Phillip Compeau & Pavel Pevzner.
Problem Title: Overlap Graph Problem
Problem ID: NEWBA3C
URL: http://rosalind.info/problems/newba3c/
'''

def main():
    """Main call to Parse, execute the functions, saves problem specific data."""
    # Read the input data.
    with open('data/Textbook_NEWBA3C_IP4.txt') as input_data:
        dna = [line.strip() for line in input_data.readlines()]

    # Temporary function(lambda) to check for overlap.
    # More info on lambda: http://www.python-course.eu/lambda.php
    check_overlap = lambda pair: pair[0][1:] == pair[1][:-1]
	
    # Temporary function(lambda) to print overlaps in the required format.
    print_overlap = lambda pair: ' -> '.join(pair)
    
    # Get all possible pairs from the input data
    pairs = ([dna1, dna2] for i, dna1 in enumerate(dna) for j, dna2 in enumerate(dna) if i != j)
	
    # Filter non-overlapping pairs and print overlapping pairs using the lambda functions
    overlaps = map(print_overlap, filter(check_overlap, pairs))

    # Print the Output.
    print '\n'.join(overlaps)
	
    # Save the Output.
    with open('output/Textbook_NEWBA3C_OP4.txt', 'w') as output_data:
        output_data.write('\n'.join(overlaps))

if __name__ == '__main__':
    main()
