'''
A solution to a "code challenges" that accompanies Bioinformatics Algorithms: An Active-Learning Approach by Phillip Compeau & Pavel Pevzner.
Problem Title: Minimum Skew Problem
Problem ID: BA1F
URL: http://rosalind.info/problems/ba1f/
'''

# Reading the input data
with open('data/Textbook_BA1F_IP2.txt') as input_data:
	dna = input_data.read().strip()

# Initilization of the variables
skew_value, min_skew, min_ind = 0, 1, []

# Calculation of the Minimum skew with their indices
for index, nucleotide in enumerate(dna):
	# Determine the skew value.
	if nucleotide == 'C':
		skew_value -= 1
	elif nucleotide == 'G':
		skew_value += 1
	# To check if there is a new minimum skew
	if skew_value == min_skew:
		min_ind.append(str(index+1))
	elif skew_value < min_skew:
		min_skew = skew_value
		min_ind = [str(index+1)]

# Print the output
print ' '.join(min_ind)

# Save the output
with open('output/Textbook_BA1F_OP2.txt', 'w') as output_data:
	output_data.write(' '.join(min_ind))
