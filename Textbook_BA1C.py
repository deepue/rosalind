'''
A solution to a "code challenges" that accompanies Bioinformatics Algorithms: An Active-Learning Approach by Phillip Compeau & Pavel Pevzner.
Problem Title: Find the Reverse Complement of a String
Problem ID: BA1C
URL: http://rosalind.info/problems/ba1c/
'''

from string import maketrans

def ReverseComplementDNA(seq):
    """Returns the Reverse Complement of the DNA."""
    transtab = maketrans('ATCG', 'TAGC')
    return seq.translate(transtab)[::-1]

def main():
    """Main call to Parse, execute the functions, saves problem specific data."""
    # Reading the input data
    with open('data/Textbook_BA1C_IP1.txt') as input_data:
        dna = input_data.read().strip()

    # Finding the Reverse Complement of the DNA
    rev_comp_dna = ReverseComplementDNA(dna)

    # Print the output
    print rev_comp_dna

    # Save the output
    with open('output/Textbook_BA1C_OP1.txt', 'w') as output_data:
        output_data.write(rev_comp_dna)

if __name__ == '__main__':
    main()

