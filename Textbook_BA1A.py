'''
A solution to a "code challenges" that accompanies Bioinformatics Algorithms: An Active-Learning Approach by Phillip Compeau & Pavel Pevzner.
Problem Title: Compute the Number of Times a Pattern Appears in a Text
Problem ID: BA1A 
URL: http://rosalind.info/problems/ba1a/
'''

# Reading the input data
with open('data/Textbook_BA1A_IP2.txt') as input_data:
	text, pattern = [line.strip() for line in input_data.readlines()]

# Pattern matching
pattern_pos = []
for i in xrange(len(text)-len(pattern)+1):
	if text[i:i+len(pattern)] == pattern:
		pattern_pos.append(str(i))

# Print the output
print len(pattern_pos)
print ' '.join(pattern_pos)

# Save the output
with open('output/Textbook_BA1A_OP2.txt', 'w') as output_data:
	output_data.write(str(len(pattern_pos)))
