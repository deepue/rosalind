'''
A solution to a "code challenges" that accompanies Bioinformatics Algorithms: An Active-Learning Approach by Phillip Compeau & Pavel Pevzner.
Problem Title: Find the Most Frequent Words in a String
Problem ID: BA1B
URL: http://rosalind.info/problems/ba1b/
'''

# Reading the input data
with open('data/Textbook_BA1B_IP2.txt') as input_data:
	dna, k = [line.strip() for line in input_data.readlines()]
	k = int(k)

# Computing the frequency of the kmers in the string
kmer_dict = dict()

for i in xrange(len(dna)-k+1):
	if dna[i:i+k] in kmer_dict:
		kmer_dict[dna[i:i+k]] += 1
	else:
		kmer_dict[dna[i:i+k]] = 1

# Identification of Most frequent kmers
kmers = [item[0] for item in kmer_dict.items() if item[1] == max(kmer_dict.values())]

# Print the output
print ' '.join(kmers)

# Save the output
with open('output/Textbook_BA1B_OP2.txt', 'w') as output_data:
	output_data.write(' '.join(kmers))
