'''
A solution to a "code challenges" that accompanies Bioinformatics Algorithms: An Active-Learning Approach by Phillip Compeau & Pavel Pevzner.
Problem Title: Pattern Matching Problem
Problem ID: BA1D
URL: http://rosalind.info/problems/ba1d/
'''

from string import maketrans

# Reading the input data
with open('data/Textbook_BA1D_IP2.txt') as input_data:
	pattern, text = [line.strip() for line in input_data.readlines()]

# Pattern matching
pattern_pos = []
for i in xrange(len(text)-len(pattern)+1):
	if text[i:i+len(pattern)] == pattern:
		pattern_pos.append(str(i))

# Print the output
print ' '.join(pattern_pos)

# Save the output
with open('output/Textbook_BA1D_OP2.txt', 'w') as output_data:
	output_data.write(' '.join(pattern_pos))
