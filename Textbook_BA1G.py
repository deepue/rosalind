'''
A solution to a "code challenges" that accompanies Bioinformatics Algorithms: An Active-Learning Approach by Phillip Compeau & Pavel Pevzner.
Problem Title: Hamming Distance Problem
Problem ID: BA1G
URL: http://rosalind.info/problems/ba1g/
'''

from itertools import imap
from operator import ne

def hamming_distance(seq1, seq2):
    'Returns the Hamming distance between two sequences.'
    if len(seq1) != len(seq2):
        raise ValueError('Sequences are of unequal length.')
    return sum(imap(ne, seq1, seq2))

def main():
    """Main call to Parse, execute the functions, saves problem specific data."""
    # Read the input data.
    with open('data/Textbook_BA1G_IP2.txt') as input_data:
        seq1, seq2 = [line.strip() for line in input_data]

    # Calculate the Hamming Distance.
    h_dist = str(hamming_distance(seq1,seq2))

    # Print the Output.
    print h_dist
	
    # Save the output
    with open('output/Textbook_BA1G_OP2.txt', 'w') as output_data:
        output_data.write(h_dist)

if __name__ == '__main__':
    main()
